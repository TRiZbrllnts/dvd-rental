#include <iostream>
#include <string.h>
#include <ctime>
#include <windows.h>
using namespace std;

void dvd()
{
HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
SetConsoleTextAttribute(hConsole, 3);  
cout <<" What are you looking for? " << endl
 << endl;	
}


int main()
{ 

	string movie[] = {
	"0",
    "Black Panther",
    "Avengers: Infinity War",
    "Incredibles 2",
    "Jurassic World: Fallen Kingdom",
    "Deadpool 2",
    "Dr. Seuss’ The Grinch",
    "Jumanji: Welcome to the Jungle",
    "Mission: Impossible—Fallout",
    "Ant-Man and the Wasp",
    "Solo: A Star Wars Story",
    "Venom",
    "A Star is Born",
    "Aquaman",
    "Bohemian Rhapsody",
    "A Quiet Place",
    "Ralph Breaks The Internet",
    "Crazy Rich Asians",
    "Hotel Transylvania 3: Summe…",
    "Halloween",
    "Fantastic Beasts: The Crime…",
    "The Meg",
    "Ocean’s 8",
    "Mary Poppins Returns",
    "Ready Player One",
    "Spider-Man: Into"};

//SYNOPSYS
    string sypnosis[] = {
    "0",
    "King T’Challa returns home to the reclusive, technologically advanced African nation of Wakanda to serve as his country’s new leader. However, T’Challa soon finds that he is challenged for the throne from factions within his own country. When two foes conspire to destroy Wakanda, the hero known as Black Panther must team up with C.I.A. agent Everett K. Ross and members of the Dora Milaje, Wakanadan special forces, to prevent Wakanda from being dragged into a world war.",
    "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment—the fate of Earth and existence itself has never been more uncertain.",
    "Everyone’s favorite family of superheroes is back, but this time Helen is in the spotlight, leaving Bob at home with Violet and Dash to navigate the day-to-day heroics of “normal” life. It’s a tough transition for everyone, made tougher by the fact that the family is still unaware of baby Jack-Jack’s emerging superpowers. When a new villain hatches a brilliant and dangerous plot, the family and Frozone must find a way to work together again—which is easier said than done, even when they’re all Incredible.",
    "It’s been four years since theme park and luxury resort Jurassic World was destroyed by dinosaurs out of containment. Isla Nublar now sits abandoned by humans while the surviving dinosaurs fend for themselves in the jungles. When the island’s dormant volcano begins roaring to life, Owen and Claire mount a campaign to rescue the remaining dinosaurs from this extinction-level event. Owen is driven to find Blue, his lead raptor who’s still missing in the wild, and Claire has grown a respect for these creatures she now makes her mission. Arriving on the unstable island as lava begins raining down, their expedition uncovers a conspiracy that could return our entire planet to a perilous order not seen since prehistoric times.",
    "After surviving a near fatal bovine attack, a disfigured cafeteria chef struggles to fulfill his dream of becoming Mayberry’s hottest bartender while also learning to cope with his lost sense of taste. Searching to regain his spice for life, as well as a flux capacitor, Wade must battle ninjas, the yakuza, and a pack of sexually aggressive canines, as he journeys around the world to discover the importance of family, friendship, and flavor—finding a new taste for adventure and earning the coveted coffee mug title of World’s Best Lover. ",
    "Each year at Christmas they disrupt his tranquil solitude with their increasingly bigger, brighter and louder celebrations. When the Whos declare they are going to make Christmas three times bigger this year, the Grinch realizes there is only one way for him to gain some peace and quiet: he must steal Christmas. To do so, he decides he will pose as Santa Claus on Christmas Eve, even going so far as to trap a lackadaisical misfit reindeer to pull his sleigh. Meanwhile, down in Who-ville, Cindy-Lou Who-a young girl overflowing with holiday cheer-plots with her gang of friends to trap Santa Claus as he makes his Christmas Eve rounds so that she can thank him for help for her overworked single mother. As Christmas approaches, however, her good-natured scheme threatens to collide with the Grinch's more nefarious one. Will Cindy-Lou achieve her goal of finally meeting Santa Claus? Will the Grinch succeed in silencing the Whos’ holiday cheer once and for all?",
    "Four teenagers in detention are sucked into the world of Jumanji. When they discover an old video game console with a game they’ve never heard of, they are immediately thrust into the game’s jungle setting, into the bodies of their avatars. What they discover is that you don’t just play Jumanji—Jumanji plays you. They’ll have to go on the most dangerous adventure of their lives, or they’ll be stuck in the game forever… ",
    "Ethan Hunt and his IMF team, along with some familiar allies are in a race against time after a mission gone wrong. ",
    "In the aftermath of “Captain America: Civil War,” Scott Lang grapples with the consequences of his choices as both a Super Hero and a father. As he struggles to re-balance his home life with his responsibilities as Ant-Man, he’s confronted by Hope van Dyne and Dr. Hank Pym with an urgent new mission. Scott must once again put on the suit and learn to fight alongside The Wasp as the team works together to uncover secrets from their past.",
    "Through a series of daring escapades deep within a dark and dangerous criminal underworld, Han Solo meets his mighty future copilot Chewbacca and encounters the notorious gambler Lando Calrissian, in a journey that will set the course of one of the Star Wars saga’s most unlikely heroes.",
    "Eddie Brock becomes the host for the alien symbiote Venom. As a journalist, Eddie has been trying to take down the notorious founder of the Life Foundation, genius Carlton Drake—and that obsession ruined his career and his relationship with his girlfriend, Anne Weying. Upon investigating one of Drake’s experiments, the alien Venom merges with Eddie’s body, and he suddenly has incredible new superpowers, as well as the chance to do just about whatever he wants. Twisted, dark, unpredictable, and fueled by rage, Venom leaves Eddie wrestling to control dangerous abilities that he also finds empowering and intoxicating. As Eddie and Venom need each other to get what they’re looking for, they become more and more intertwined—where does Eddie end and Venom begin?",
    "Jackson Maine discovers—and falls in love with—struggling artist Ally. She has just about given up on her dream to make it big as a singer… until Jack coaxes her into the spotlight. But even as Ally’s career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
    "The Origin story of half-human, half-Atlantean Arthur Curry and takes him on the journey of his lifetime—one that will not only force him to face who he really is, but to discover if he is worthy of who he was born to be…a king. ",
    "A celebration of Queen, their music and their lead singer Freddie Mercury, who defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet. The film traces the meteoric rise of the band through their iconic songs and revolutionary sound, their near-implosion as Mercury’s lifestyle spirals out of control, and their triumphant reunion on the eve of Live Aid, where Mercury, facing a life-threatening illness, leads the band in one of the greatest performances in the history of rock music. In the process, cementing the legacy of a band that were always more like a family, and who continue to inspire outsiders, dreamers and music lovers to this day. ",
    "A family of four must navigate their lives in silence after mysterious creatures that hunt by sound threaten their survival. If they hear you, they hunt you.",
    "Video-game bad guy Ralph and best friend Vanellope von Schweetz leave the comforts of Litwak’s arcade in an attempt to save her game, Sugar Rush. Their quest takes them to the vast, uncharted world of the internet where they rely on the citizens of the internet—the Netizens—to help navigate their way. Lending a virtual hand are Yesss, the head algorithm and the heart and soul of the trend-making site “BuzzzTube,” and Shank, a tough-as-nails driver from a gritty online auto-racing game called Slaughter Race, a place Vanellope wholeheartedly embraces—so much so that Ralph worries he may lose the only friend he’s ever had.",
    "Native New Yorker Rachel Chu accompanies her longtime boyfriend, Nick Young, to his best friend’s wedding in Singapore. Excited about visiting Asia for the first time but nervous about meeting Nick’s family, Rachel is unprepared to learn that Nick has neglected to mention a few key details about his life. It turns out that he is not only the scion of one of the country’s wealthiest families but also one of its most sought-after bachelors. Being on Nick’s arm puts a target on Rachel’s back, with jealous socialites and, worse, Nick’s own disapproving mother taking aim. And it soon becomes clear that while money can’t buy love, it can definitely complicate things. ",
    "Welcome to Hotel Transylvania, Dracula’s lavish five-stake resort, where monsters and their families can live it up and no humans are allowed. One special weekend, Dracula has invited all his best friends—Frankenstein and his wife, the Mummy, the Invisible Man, the Werewolf family, and more—to celebrate his beloved daughter Mavis’s 118th birthday. For Dracula catering to all of these legendary monsters is no problem but the party really starts when one ordinary guy stumbles into the hotel and changes everything! ",
    "Laurie Strode comes to her final confrontation with Michael Myers, the masked figure who has haunted her since she narrowly escaped his killing spree on Halloween night four decades ago. ",
    "At the end of the first film, the powerful Dark wizard Gellert Grindelwald was captured by MACUSA (Magical Congress of the United States of America), with the help of Newt Scamander. But, making good on his threat, Grindelwald escaped custody and has set about gathering followers, most unsuspecting of his true agenda: to raise pure-blood wizards up to rule over all non-magical beings. In an effort to thwart Grindelwald’s plans, Albus Dumbledore enlists his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided Wizarding World. ",
    "A deep-sea submersible—part of an international undersea observation program—is attacked by a massive creature, previously thought to be extinct, and now lies disabled at the bottom of the deepest trench in the Pacific…with its crew trapped inside. With time running out, expert deep sea rescue diver Jonas Taylor is recruited by a visionary Chinese oceanographer, against the wishes of his daughter Suyin, to save the crew—and the ocean itself—from this unstoppable threat: a pre-historic 75-foot-long shark known as the Megalodon. What no one could have imagined is that, years before, Taylor had encountered this same terrifying creature. Now, teamed with Suyin, he must confront his fears and risk his own life to save everyone trapped below…bringing him face to face once more with the greatest and largest predator of all time.",
    "Upon her release from prison, Debbie, the estranged sister of legendary conman Danny Ocean, puts together a team of unstoppable crooks to pull of the heist of the century. Their goal is New York City’s annual Met Gala, and a necklace worth in excess of 150 million dollars. ",
    "The mysterious Mary Poppins returns to Depression-era London to visit Jane and her brother Michael, now a father of three, and helps them rediscover the joy they knew as children.",
    "In the year 2045, the real world is a harsh place. The only time Wade Watts truly feels alive is when he escapes to the OASIS, an immersive virtual universe where most of humanity spend their days. In the OASIS, you can go anywhere, do anything—the only limits are your imagination. The OASIS was created by the brilliant and eccentric James Halliday, who left his immense fortune and total control of the OASIS to the winner of a three-part contest he designed to find a worthy heir. When Wade conquers the first challenge of the reality-bending treasure hunt, he and his friends—known as the High Five—are hurled into a fantastical universe of discovery and danger to save the OASIS and their world.",
    "“Spider-Man: Into the Spider-Verse” introduces Brooklyn teen Miles Morales, and the limitless possibilities of the Spider-Verse, where more than one can wear the mask."};

//ACTORS
	string actors[] = {
	"0",
    "Chadwick Boseman as T’Challa/Black Panther, Michael B. Jordan as Erik Killmonger, Lupita Nyong'o as Nakia",
    "Robert Downey, Jr. as Tony Stark/Iron Man, Chris Hemsworth as Thor, Mark Ruffalo as Bruce Banner/Hulk",
    "Craig T. Nelson as Bob Parr / Mr. Incredible, Holly Hunter as Helen Parr / Elastigirl, Sarah Vowell as Violet Parr",
    "Chris Pratt as Owen Grady, Bryce Dallas Howard as Claire Dearing",
    "Ryan Reynolds as Wade Wilson / Deadpool, Josh Brolin as Nathan Summers/Cable, Morena Baccarin as Vanessa",
    "Benedict Cumberbatch as Grinch",
    "Dwayne Johnson as Dr. Smolder, Bravestone/Spencer's Avatar",
    "Tom Cruise as Ethan Hunt",
    "Paul Rudd as Scott Lang/Ant-Man",
    "Alden Ehrenreich as Han Solo",
    "Tom Hardy as Eddie Brock/Venom",
    "Bradley Cooper as Jack, Lady Gaga as Ally",
    "Jason Momoa as Arthur Curry/Aquaman, Amber Heard as Mera",
    "Rami Malek as Freddie Mercury",
    "Emily Blunt as Evelyn Abbott, John Krasinski as Lee Abbott",
    "John C. Reilly as Wreck-It Ralph, Sarah Silverman as Vanellope Von Schweetz",
    "Constance Wu as Rachel Chu, Henry Golding as Nick Young",
    "Adam Sandler as Count Dracula, Andy Samberg as Jonathan “Johnny” Loughran",
    "Jamie Lee Curtis as Laurie Strode",
    "Johnny Depp as Gellert Grindelwald, Eddie Redmayne as Newton “Newt” Scamander",
    "Jason Statham as Jonas Taylor, Li Bingbing as Suyin Zhang",
    "Sandra Bullock as Debbie Ocean, Cate Blanchett as Lou",
    "Emily Blunt as Mary Poppinis",
    "Tye Sheridan as Wade Watts/Parzival",
    "Shameik Moore as Miles Morales / Spider-Man, Jake Johnson as Peter B. Parker/Spider-Man"};
    
////DIRECTORS
	string director[] = {
	"0",
	"Ryan Coogler",
    "Joe Russo  & Anthony Russo",
    "Brad Bird",
    "J.A. Bayona",
    "David Leitch",
    "Scott Mosier",
    "Jake Kasdan",
    "Christopher McQuarrie",
    "Peyton Reed",
    "Ron Howard",
    "Ruben Fleischer",
    "Bradley Cooper",
    "Peter Safran",
    "Bryan Singer",
    "John Krasinski",
    "Rich Moore",
    "Jon M. Chu",
    "Genndy Tartakovsky",
    "David Gordon Green",
    "David Yates",
    "Jon Turteltaub", 
    "Gary Ross",
    "Rob Marshall",
    "Steven Spielberg",
    "Bob Persichetti"};

///PRODUCERS
string producers[] = {
	"0",
    "Kevin Feige",
    "Kevin Feige",
    "John Walker & Nicole Paradis Grindle",
    "Frank Marshall, Patrick Crowley & Belen Atienza",
    "Simon Kinberg, Lauren Shuler Donner, & Ryan Reynolds",
    "Christopher Meledandri",
    "Matt Tolmach",
    "Christopher McQuarrie",
    "Kevin Feige",
    "Kathleen Kennedy",
    "Avi Arad",
    "Jon Peters",
    "James Wan",
    "Graham King", 
    "Michael Bay",
    "Clark Spencer", 
    "Nina Jacobson",
    "Michelle Murdocca", 
    "Malek Akkad",
    "David Heyman",
    "Lorenzo di Bonaventura",
    "Steven Soderbergh",
    "Rob Marshall",
    "Steven Spielberg",
    "Phil Lord"};
	
	
	string dvdstatus[] = {"0","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  ","AVAILABLE  "};
	string customers[41];
	string dvdrented[41];
	string daterented[41];
	string customername;
	int selecteddvd;
	bool x2 = true;
	int optionchoice;
	int dvdoptionchoice;
	int xc3=0;
	
	//MAIN FUNCTION 
	while(x2 = true)
	{
		system("CLS");
		dvd();
		//SetConsoleTextAttribute(hConsole, 10);                                                                                

		cout<<// "RECORDS :" << xc3 << endl << endl
		
			 "           OPTIONS:    \n              " << endl 
			<< "  1 Movie list                " << endl
			<< "  2 Movie Description              " << endl
			<< "  3 Rent DVD                " << endl 
			<< "  4 Return DVD                     " << endl 
			<< "  5 Close                          " << endl 
			
			<< "\n Enter a number assigned to your chosen option: ";
			
		cin >> dvdoptionchoice;
		if(cin.fail())
		{
		
			system("CLS");
			cout << "INVALID INPUT" << endl;
			system("PAUSE");
			system("CLS");
		}
		
		if (dvdoptionchoice == 1)
		{ 
			system("CLS");
			dvd();
			cout << "Availability:    List of Movie:\n" << endl;
			for(int x1=1;x1<=25;x1++)
				{
					cout << dvdstatus[x1] << "      " << x1 << ": "<< movie[x1] << endl;
				}
			system("PAUSE");
		}
		else if (dvdoptionchoice == 2)
		{	
			system("CLS");
			dvd();
			cout << "Availability:           List of Movie:\n" << endl;
			for(int x1=1;x1<=25;x1++)
				{
					cout << dvdstatus[x1] << "            " << x1 << ": "<< movie[x1] << endl;
				}
				
			cout << "\nTo view the film description input its assigned number: \n";
			cin >> selecteddvd;
		
		
			if(cin.fail() or selecteddvd <= 0 or selecteddvd >=26)
			{
		
				cin.clear();
	     		cin.ignore();
			
				system("PAUSE");
				system("CLS");
			}
			else
			{
				system("CLS");
				dvd();
				cout << "Movie Title: " << movie[selecteddvd] << endl 
					 << "\nAvailability: " << dvdstatus[selecteddvd] << endl
					 << "\nDirector: " << director[selecteddvd] << endl 
					 << "\nSypnosis:" << sypnosis[selecteddvd] << endl
					 << "\nActors/Actresses: " << actors[selecteddvd] << endl
					 << "\nProducers: " << producers[selecteddvd] << endl 
					 << endl;
				system("PAUSE");
			}
		}

		else if (dvdoptionchoice == 3)
		{
			system("CLS");
			dvd();

			cout << "Availability:         List of Movie:\n"  << endl;
			for(int x1=1;x1<=25;x1++)
				{
					cout << dvdstatus[x1] << "        " << x1 << ": "<< movie[x1] << endl;
				}
		
					///////////NUMBER
					bool r = true;
					while(r == true)
					{
						cout << "\n\nAlready found the perfect movie? [To rent the DVD input the designated number]: ";
						cin >> selecteddvd;
						if(cin.fail() or selecteddvd <= 0 or selecteddvd >=26)
						{
							cout << "INVALID INPUT" << endl;
							cin.clear();
							cin.ignore();
							r = true;
						}
						else
						{
							r = false;
						}
					}
					if (dvdstatus[selecteddvd] == "UNAVAILABLE")
					{
						cout << "\nThe film you wanted was currently unavailable, try to come back later" << endl;
						system("PAUSE");
					}
					else
					{
					//	SetConsoleTextAttribute(hConsole, 2);  
						////////////NAME
						bool p = true;
						while(p == true)
						{
							cout << "\nEnter your Name {fullname and to use space input an underscore}: ";
							cin >> customers[xc3];
							if(cin.fail())
							{
								cin.clear();
								p = true;
							}
							else
							{
								p = false;
							}
						}
						dvdrented[xc3] = movie[selecteddvd];
	
						time_t curr_time;
						tm * curr_tm;
						char date_string[100];
						time(&curr_time);
						curr_tm = localtime(&curr_time);
						strftime(date_string, 50, "%B %d, %Y", curr_tm);
						daterented[xc3] = date_string;
	
					
						cout << "Borrower's Name': " << customers[xc3] << endl;
						cout << "Movie rented: " << dvdrented[xc3] << endl;
						cout << "Date of Transaction: " << daterented[xc3] << endl;
						system("PAUSE");
						xc3=++xc3;
						dvdstatus[selecteddvd] = "UNAVAILABLE";
					}
		}
	
		else if (dvdoptionchoice == 4)
		{
			dvd();
			system("CLS");
			cout << "Availability:         List of Movie:\n" << endl;
			for(int x1=1;x1<=25;x1++)
				{
					cout << dvdstatus[x1] << "          " << x1 << ": "<< movie[x1] << endl;
				}
					bool r = true;
					while(r == true)
					{
						cout << "\nTo be able to return a DVD, input its assigned number: ";
						cin >> selecteddvd;
						if(cin.fail() or selecteddvd <= 0 or selecteddvd >=26)
						{
							
							cout << "INVALID INPUT" << endl;
							cin.clear();
							cin.ignore();
							r = true;
						}
						else
						{
							r = false;
						}
					}
					if (dvdstatus[selecteddvd] == "AVAILABLE  ")
					{
						cout << "The disc you wish to return is currently available, your action is voided" << endl;
						system("PAUSE");
					}
					else
					{
				//		SetConsoleTextAttribute(hConsole, 2);  
						dvdstatus[selecteddvd] = "AVAILABLE  ";
						cout << "Thank you for the patronage, the DVD was returned safe HAHA" << endl;
						system("PAUSE");
					}
			
		}
	
		else if (dvdoptionchoice == 5)
		{
			
			exit(0);
		}
		else
		{
		//	SetConsoleTextAttribute(hConsole, 4);  
			cout << "INVALID INPUT" << endl;
			system("PAUSE");
		}
	}
}
